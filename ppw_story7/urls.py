from django.urls import path, include
from .views import accordion

urlpatterns = [
    path('accordion', accordion, name='accordion'),
]