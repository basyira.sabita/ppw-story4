$( document ).ready(function() {
    $( "#accordion" ).accordion({
        collapsible: true,
        heightStyle: "content"
    });

    $('i.up').click(function(){
        var $parent = $(this).closest("li");
        if ($parent.prev() != 0) {
            $parent.insertBefore($parent.prev("li"));
        }
        return false;
    });

    $('i.down').click(function(){
        var $parent = $(this).closest("li");
        $parent.insertAfter($parent.next());
        return false;
    });

});
