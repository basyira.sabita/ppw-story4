from django.test import TestCase, Client

# Create your tests here.
def test_url_story7(self):
    response = Client().get('/accordion/')
    self.assertEquals(200, response.status_code)

def test_template_story7(self):
    response = Client().get('/form-kegiatan/')
    self.assertTemplateUsed(response, 'story7.html')

def test_views_story7(self):
    response = Client().get('/form-kegiatan/')
    isi_html = response.content.decode('utf8')
    self.assertIn("ACCORDION", isi_html)
    self.assertIn("AKTIVITAS SAAT INI", isi_html)
    self.assertIn("HARAPAN", isi_html)
    self.assertIn("PRESTASI", isi_html)
    self.assertIn("ORGANISASI/KEPANITIAAN", isi_html)