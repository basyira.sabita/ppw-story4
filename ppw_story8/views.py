from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
def cariBuku(request):
    return render(request, 'cariBuku.html')

def dataBuku(request):
    key = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)