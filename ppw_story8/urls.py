from django.urls import path, include
from . import views

urlpatterns = [
    path('cari-buku', views.cariBuku, name='buku'),
    path('data', views.dataBuku, name="data")
]