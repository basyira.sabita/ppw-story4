$('#keyword').keyup( function() {
    var value_keyword = $("#keyword").val();
    //console.log(value_keyword);
    var url_keyword = '/data?q=' + value_keyword;
    //console.log(url_keyword);
    $.ajax({
        url : url_keyword,
        success : function(hasil) {
            console.log(hasil);
            
            var obj_data = $('#data');
            obj_data.empty();

            var table = "";
            for (i = 0; i < hasil.items.length; i++) {
                var judul = hasil.items[i].volumeInfo.title;
                var penulis = hasil.items[i].volumeInfo.authors;
                var deskripsi = hasil.items[i].volumeInfo.description;
                var cover = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                //console.log(judul);
                table += ('<tr>'+ '<td>'+ judul + '</td>'+'<td>'+ penulis +'</td>'); //+'</tr>'???

                //obj_data.append('<tr>'+ '<td>'+ judul + '</td>' + '<td>'+ penulis +'</td>' + '<td>'+ deskripsi + '</td>' + '<td><img src='+ cover + '></td>' + '</tr>');

                if (deskripsi == undefined) {
                    table += ('<td>'+'Belum ada deskripsi untuk buku ini' +'</td>');
                } else {
                    if (deskripsi.length > 300) {
                        table += ('<td>'+deskripsi.substring(0,300) + " ..." +'</td>');
                    } else {
                        table += ('<td>'+deskripsi +'</td>');
                    }
                }

                if (cover == null) {
                    table += ('<td>'+ "Tidak ada preview cover buku" +'</td>'+'</tr>');
                } else {
                    table += ('<td>'+'<img src='+ cover +'></td>'+'</tr>');
                }
          }
          obj_data.append(table)
        }
    });
});