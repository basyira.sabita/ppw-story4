from django.test import TestCase

# Create your tests here.
def test_url_story8(self):
    response = Client().get('/cari-buku/')
    self.assertEquals(200, response.status_code)

def test_template_story7(self):
    response = Client().get('/cari-buku/')
    self.assertTemplateUsed(response, 'cariBuku.html')