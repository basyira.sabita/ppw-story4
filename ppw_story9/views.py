from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib import messages
# Create your views here.
def logIn(request) :
    if request.method == 'POST' :
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/greetings')
        return render(request, 'login.html', {'form':form})
    else :
        form = AuthenticationForm()
        return render(request, 'login.html', {'form':form})

def logOut(request):
    logout(request)
    return redirect('/')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
           user = form.save()
           login(request, user)
           return redirect('/greetings')
        else:
            return render(request, 'register.html', {'form':form})
    else:
        form = UserCreationForm()
    return render(request, 'register.html', {'form':form})

def greetings(request):
    return render(request, 'greetings.html')