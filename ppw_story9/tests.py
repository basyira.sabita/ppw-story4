from django.test import TestCase
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

# Create your tests here.
def test_url_login(self):
    response = Client().get('/login/')
    self.assertEquals(200, response.status_code)

def test_url_register(self):
    response = Client().get('/register/')
    self.assertEquals(200, response.status_code)

def test_url_greetings(self):
    response = Client().get('/greetings/')
    self.assertEquals(200, response.status_code)

def test_template_login(self):
    response = Client().get('/login/')
    self.assertTemplateUsed(response, 'login.html')

def test_template_greetings(self):
    response = Client().get('/greetings/')
    self.assertTemplateUsed(response, 'greetings.html')

def test_template_register(self):
    response = Client().get('/register/')
    self.assertTemplateUsed(response, 'register.html')

def test_login_user(self) :
    self.credentials = {'username': "usercoba", 'password':'pass0987'}
    User.objects.create_user(**self.credentials)
    form = AuthenticationForm(data=self.credentials)
    self.assertTrue(form.is_valid())
