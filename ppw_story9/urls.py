from django.urls import path, include
from . import views

urlpatterns = [
    path('login', views.logIn),
    path('register', views.register),
    path('greetings', views.greetings),
    path('logout', views.logOut),
]