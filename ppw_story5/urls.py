from django.urls import path
from . import views

app_name = 'ppw_story5'
urlpatterns = [
    path('add', views.formMatkul, name="form_url"),
    path('detail/<int:id>', views.detailMatkul, name="detail_url"),
    path('list-matkul', views.dataMatkul, name="listmatkul_url"),
]
