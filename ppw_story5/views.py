from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import MataKuliah
from .forms import FormMatkul

# Create your views here.
def formMatkul(request):
    form = FormMatkul(request.POST or None)
    data = MataKuliah.objects.all()
    if (form.is_valid() and request.method == 'POST') :
        form.save()
        return HttpResponseRedirect('/list-matkul')

    else :
        return render(request, 'form_matkul.html', {'form' : form, 'data' : data})

def dataMatkul(request):
    data = MataKuliah.objects.all()
    if (request.method == 'POST'):
        MataKuliah.objects.get(id=request.POST['id']).delete()
    return render(request, 'data_matkul.html', {'data':data})
    
def detailMatkul(request, id):
    data = MataKuliah.objects.get(pk=id)
    if (request.method == 'POST'):
        MataKuliah.objects.get(id=request.POST['id']).delete()
        return HttpResponseRedirect('/list-matkul')
    return render(request, 'detail_matkul.html', {'data':data})