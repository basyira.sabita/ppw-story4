from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
SEMESTER_CHOICES = (
    ("Gasal 2020/2021","Gasal 2020/2021"),
    ("Genap 2019/2020","Genap 2019/2020"),
    ("Gasal 2019/2020","Gasal 2019/2020"),
    ("Lainnya","Lainnya")
)
class MataKuliah(models.Model):
    nama_matkul = models.CharField('Mata Kuliah', max_length = 120, null = True)
    dosen_pengajar = models.CharField('Dosen Pengajar', max_length = 120, null = True)
    jmlh_sks = models.IntegerField('Jumlah SKS',
        validators = [
            MaxValueValidator(24),
            MinValueValidator(1)
        ]
    )
    deskripsi = models.CharField('Deskripsi', max_length = 500)
    semester_tahun = models.CharField('Semester Tahun', max_length = 15, choices = SEMESTER_CHOICES)
    ruang_kuliah = models.CharField('Ruang Kelas', max_length = 120, null = True)

    def __str__(self):
        return f'{self.nama_matkul}'
