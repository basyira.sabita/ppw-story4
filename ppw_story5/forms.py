from django.forms import ModelForm
from .models import MataKuliah

class FormMatkul(ModelForm):
    def __init__ (self, *args, **kwargs) :
        super().__init__(*args, **kwargs)
        for _, value in self.fields.items():
            value.widget.attrs['placeholder'] = value.help_text
    
    class Meta:
        model = MataKuliah
        fields = '__all__'