from django.urls import path, include
from .views import story_1

urlpatterns = [
    path('', story_1, name='story_1'),
]
