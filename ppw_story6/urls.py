from django.urls import path
from ppw_story6.views import formKegiatan, listKegiatan

urlpatterns = [
    path('form-kegiatan/', formKegiatan),
    path('list-kegiatan/', listKegiatan),
]
