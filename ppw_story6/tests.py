from django.test import TestCase, Client

# Create your tests here.
class TestStory6(TestCase):

    def test_url_form_kegiatan(self):
        response = Client().get('/form-kegiatan/')
        self.assertEquals(200, response.status_code)

    def test_template_form_kegiatan(self):
        response = Client().get('/form-kegiatan/')
        self.assertTemplateUsed(response, 'form_kegiatan.html')
    
    def test_views_form_kegiatan(self):
        response = Client().get('/form-kegiatan/')
        isi_html = response.content.decode('utf8') #di decode karena response.content balikin bytestring

        self.assertIn("ISI KEGIATAN", isi_html)
        self.assertIn('<input id="kegiatan" type="text" name="kegiatan" placeholder="Masukkan kegiatan">', isi_html) 
        self.assertIn('<button id="kirim" type="submit" name="kirim">Submit</button>', isi_html)
        self.assertIn('<form action="/list-kegiatan" method="POST">', isi_html)
        self.assertIn('<a href="/list-kegiatan"><button>Lihat Kegiatan</button></a>', isi_html)
    
    def test_url_list_kegiatan(self):
        response = Client().get('/list-kegiatan/')
        self.assertEquals(200, response.status_code)

    def test_template_list_kegiatan(self):
        response = Client().get('/list-kegiatan/')
        self.assertTemplateUsed(response, 'list_kegiatan.html')
    
    def test_views_list_kegiatan(self):
        response = Client().get('/list-kegiatan/')
        isi_html = response.content.decode('utf8') #di decode karena response.content balikin bytestring
        self.assertIn("LIST KEGIATAN", isi_html)
        self.assertIn('<a href="/form-kegiatan"><button>Isi Kegiatan</button></a>', isi_html)
