from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def formKegiatan(request):
    response = {}
    return render(request, 'form_kegiatan.html', response)

def listKegiatan(request):
    response = {}
    return render(request, 'list_kegiatan.html', response)