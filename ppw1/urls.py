from django.urls import path, include
from .views import story3_1, story3_2, story3_3, story3_4

urlpatterns = [
    path('', story3_1, name='home'),
    path('profil', story3_2, name='profil'),
    path('portfolio', story3_3, name='portfolio'),
    path('cv', story3_4, name='cv'),
]
