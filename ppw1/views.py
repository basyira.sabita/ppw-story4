from django.shortcuts import render

# Create your views here.
def story3_1(request):
    return render(request,"pages/story3_1.html")

def story3_2(request):
    return render(request,"pages/story3_2.html")

def story3_3(request):
    return render(request, "pages/story3_3.html")

def story3_4(request):
    return render(request, "pages/story3_4.html")